<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                    <h1>Welcome <?php echo e(Auth::user()->name); ?></h1>
                    <h2>Your point is <?php echo e(Auth::user()->point); ?></h2>
                    <hr>
                    <h4>Deposit points</h4>
                    <p>
                        <a href="/deposit/20" class="btn btn-primary">20 แต้ม</a>
                        <a href="/deposit/150" class="btn btn-primary">150 แต้ม</a>
                        <a href="/deposit/300" class="btn btn-primary">300 แต้ม</a>
                        <a href="/deposit/500" class="btn btn-primary">500 แต้ม</a>
                    </p>
                    <hr>
                    <h4>Withdraw points</h4>
                    <p>
                        <a href="/withdraw/20" class="btn btn-primary">20 แต้ม</a>
                        <a href="/withdraw/150" class="btn btn-primary">150 แต้ม</a>
                        <a href="/withdraw/300" class="btn btn-primary">300 แต้ม</a>
                        <a href="/withdraw/500" class="btn btn-primary">500 แต้ม</a>
                    </p>
                    <h4>Transfer Point</h4>
                    <form action="/transfer" method="post">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <div class="row">
                              <div class="form-group col-sm-5">
                                <label for="acc_number">Account Number</label>
                                <input type="text" class="form-control" name="acc_number" id="acc_number" placeholder="Enter Account Number">
                              </div>
                              <div class="form-group  col-sm-5">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" name="amount" id="amount" placeholder="Enter Amount">
                              </div>
                              <div class="form-group  col-sm-2">
                              <button type="submit" class="btn btn-primary">Transfer</button>
                              </div>
                        </div>
                    </form>
                    <table class="table table-bordered">
                        <tr>
                            <th>Timestamp</th>
                            <th>Action</th>
                        </tr>
                        <?php $__empty_1 = true; $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <tr>
                                <td>
                                    <?php echo e($log->created_at); ?>

                                </td>
                                <td>
                                <?php if($log->action == "deposit"): ?>
                                    Deposit +<?php echo e($log->amount); ?> point.
                                <?php elseif($log->action == "withdraw"): ?>
                                    Withdraw -<?php echo e($log->amount); ?> point.
                                <?php elseif($log->action == "transfer"): ?>
                                    Transfer to <?php echo e($log->receiver_acc); ?> amount -<?php echo e($log->amount); ?> point.
                                <?php elseif($log->action == "receive"): ?>
                                    Receive from <?php echo e($log->receiver_acc); ?> amount +<?php echo e($log->amount); ?> point.
                                <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="3" align="center">
                                No transaction
                            </td>
                        </tr>
                        <?php endif; ?>

                        <?php if($logs->lastPage() > 1): ?>
                        <tr>
                            <td colspan="3">
                               <?php echo e($logs->links()); ?>

                            </td>
                        </tr>
                        <?php endif; ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/komphet/dev/laravel/resources/views/home.blade.php ENDPATH**/ ?>