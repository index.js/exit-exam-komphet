<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Log;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $cur_user = \Auth::user();
        if($cur_user->point - $request->amount > 0){
            $user = User::where("acc_number",str_replace("-","",$request->acc_number))->first();
            if(!is_null($user)){
                $cur_user->point = $cur_user->point - $request->amount;
                $cur_user->save();
                $user->point = $user->point + $request->amount;
                $user->save();
                $log = new Log();
                $log->action = "transfer";
                $log->user_id = $cur_user->id;
                $log->receiver_acc = $user->acc_number;
                $log->amount = $request->amount;
                $log->save();

                $log = new Log();
                $log->action = "receive";
                $log->user_id = $user->id;
                $log->receiver_acc = $cur_user->acc_number;
                $log->amount = $request->amount;
                $log->save();

                $request->session()->flash("status","Transfer to account ".$request->acc_number." amount ".$request->amount." successfully!");

            } else {
                $request->session()->flash("status","Account number is invalid!");
            }
        } else {
            $request->session()->flash("status","Not enough balance points!");
        }
        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
